#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>
//#include "omp.h"

typedef struct {
    unsigned w, h;
    double *data;
} Matrix;
const double eps = 1e-6;

int matrix_load(Matrix *matrix, const char *filename);
int matrix_save(Matrix *matrix, const char *filename);
int matrix_free(Matrix *matrix);
int matrix_rang(Matrix *matrix);

int main(int argc, char *argv[]) {
    if (argc != 2) {
        fprintf(stderr, "error: Input file is not specified!\n");
        return 1;
    }
    printf("Load matrix from %s\n", argv[1]);
    Matrix matrix = {0};
    if (matrix_load(&matrix, argv[1])) {
        fprintf(stderr, "error: Can't open file to read\n");
        return 2;
    }
    printf("Matrix<%u, %u> loaded\n", matrix.h, matrix.w);

    struct timespec start_time, end_time;

    clock_gettime(CLOCK_MONOTONIC, &start_time);
    int rang = matrix_rang(&matrix);
    printf("Rang: %d\n", rang);
    clock_gettime(CLOCK_MONOTONIC, &end_time);

    double calculation_time = (end_time.tv_sec - start_time.tv_sec) + (end_time.tv_nsec - start_time.tv_nsec) / 1e+9;
    printf("Time: %lg\n", calculation_time);
//    matrix_save(&matrix, "output.txt");

    // For easy result aggregation
    int n_threads = -1;
    const char *omp_threads_env = getenv("OMP_NUM_THREADS");
    if (omp_threads_env != NULL) {
        sscanf(omp_threads_env, "%d", &n_threads);  // NOLINT
    }
    fprintf(stderr, "%d,%u,%u,%lg,%s,%d\n", n_threads, matrix.h, matrix.w, calculation_time, argv[1], rang);

    matrix_free(&matrix);
    return 0;
}

int matrix_load(Matrix *matrix, const char *filename) {
    FILE *fin = fopen(filename, "r");
    if (!fin) {
        return 1;  // can't open file
    }
    matrix_free(matrix);
    fscanf(fin, "%u%u", &matrix->h, &matrix->w); // NOLINT
    matrix->data = calloc(matrix->h * matrix->w, sizeof(double));
    for (unsigned i = 0; i < matrix->h; i++) {
        for (unsigned j = 0; j < matrix->w; j++) {
            fscanf(fin, "%lg", &matrix->data[i * matrix->w + j]); // NOLINT
        }
    }
    fclose(fin);
    return 0;
}

int matrix_save(Matrix *matrix, const char *filename) {
    FILE *fout = fopen(filename, "w");
    if (!fout) {
        return 1;  // can't open file
    }
    fprintf(fout, "%u %u\n", matrix->h, matrix->w);
    for (unsigned i = 0; i < matrix->h; i++) {
        for (unsigned j = 0; j < matrix->w; j++) {
            if (j > 0) fprintf(fout, " ");
            fprintf(fout, "%lg", matrix->data[i * matrix->w + j]);
        }
        fprintf(fout, "\n");
    }
    fclose(fout);
    return 0;
}

int matrix_free(Matrix *matrix) {
    if (matrix->data) {
        free(matrix->data);
        matrix->data = 0;
    }
    return 0;
}

int matrix_rang(Matrix *matrix) {
    int rang = matrix->w;
    char *used = calloc(matrix->h, 1);
    for (unsigned j = 0; j < matrix->w; j++) {  // iterate over columns
        unsigned i;
        for (i = 0; i < matrix->h; i++) {  // iterate over rows
            if (!used[i] && fabs(matrix->data[i * matrix->w + j]) > eps) break;
        }
        if (i == matrix->h) {
            rang--;
        } else {
            used[i] = 1;
            #pragma omp parallel for
            for (unsigned l = 0; l < matrix->h; l++) {  // iterate over rows
                if (used[l] || fabs(matrix->data[l * matrix->w + j]) < eps) continue;  // Already used or 0
                double x = matrix->data[l * matrix->w + j] / matrix->data[i * matrix->w + j];
                for (unsigned p = j + 1; p < matrix->w; p++) {  // iterate over columns
                    matrix->data[l * matrix->w + p] -= x * matrix->data[i * matrix->w + p];
                }
            }
        }
    }
    free(used);
    return rang;
}
