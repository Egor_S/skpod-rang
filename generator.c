#include <stdio.h>
#include <stdlib.h>
#include <math.h>
//#include "omp.h"

typedef struct {
    unsigned w, h;
    double *data;
} Matrix;
const double eps = 1e-9;

int matrix_save(Matrix *matrix, const char *filename);
int matrix_free(Matrix *matrix);

int main(int argc, char *argv[]) {
    if (argc != 5) {
        fprintf(stderr, "Required arguments: <height> <width> <rang> <filename>\n");
        return 1;
    }
    unsigned h = (unsigned)strtoul(argv[1], NULL, 10);  // height
    unsigned w = (unsigned)strtoul(argv[2], NULL, 10);  // width
    unsigned r = (unsigned)strtoul(argv[3], NULL, 10);  // rang
    Matrix mat = {w, h, calloc(h * w, sizeof(double))};

    double *basis = calloc(r * w, sizeof(double));
    for (unsigned j = 0; j < w; j++) {
        basis[(j % r) * w + j] = 1;
    }
//    #pragma omp parallel for
    for (unsigned i = 0; i < h; i++) {  // rows
        for (unsigned p = 0; p < r; p++) {  // basis
            int x = rand() % 33 - 16;  // NOLINT
            for (unsigned j = 0; j < w; j++) {  // columns
                mat.data[i * w + j] += x * basis[p * w + j];
            }
        }
    }
    free(basis);

    matrix_save(&mat, argv[4]);
    matrix_free(&mat);
    return 0;
}

int matrix_save(Matrix *matrix, const char *filename) {
    FILE *fout = fopen(filename, "w");
    if (!fout) {
        return 1;  // can't open file
    }
    fprintf(fout, "%u %u\n", matrix->h, matrix->w);
    for (unsigned i = 0; i < matrix->h; i++) {
        for (unsigned j = 0; j < matrix->w; j++) {
            if (j > 0) fprintf(fout, " ");
            fprintf(fout, "%lg", matrix->data[i * matrix->w + j]);
        }
        fprintf(fout, "\n");
    }
    fclose(fout);
    return 0;
}

int matrix_free(Matrix *matrix) {
    if (matrix->data) {
        free(matrix->data);
        matrix->data = 0;
    }
    return 0;
}
