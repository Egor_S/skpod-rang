#!/bin/bash

TIME=${2:-00:05:00}

for i in 1 2 3
do
  for n in 1 2 4 8 16 32
  do
    mpisubmit.bg -n ${n} -w ${TIME} --stderr logs/err_$1.${n}.${i} --stdout logs/out_$1.${n}.${i} rang_mpi -- data/bigdata/$1.txt
  done
done