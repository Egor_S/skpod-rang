#include <stdlib.h>
#include <memory.h>
#include <stdio.h>
#include <math.h>
#include "mpi.h"

enum {
    SIZE_TAG = 0x10,
    STRIP_TAG = 0x11,
    SEARCH_TAG = 0x12
};
const double eps = 1e-6;

typedef struct {
    unsigned w, h;
    double *data;
} Matrix;

int matrix_load(Matrix *matrix, const char *filename);
int matrix_save(Matrix *matrix, const char *filename);
int matrix_free(Matrix *matrix);
int rang_mpi_worker(int mpi_rank, int mpi_size);

int main (int argc, char *argv[]) {
    if (argc != 2) {
        fprintf(stderr, "error: Input file is not specified!\n");
        return 1;
    }
    int mpi_size, mpi_rank;
    MPI_Init(&argc, &argv);
    MPI_Comm_size(MPI_COMM_WORLD, &mpi_size);
    MPI_Comm_rank(MPI_COMM_WORLD, &mpi_rank);

    Matrix matrix = {0};
    unsigned shape[2];
    int rang = 0;
    double calculation_time = MPI_Wtime();

    if (mpi_rank == 0) {
        printf("Load matrix from %s\n", argv[1]);
        if (matrix_load(&matrix, argv[1])) {
            fprintf(stderr, "error: Can't open file to read\n");
            return 2;
        }
        printf("Matrix<%u, %u> loaded\n", matrix.h, matrix.w);

        MPI_Request *requests = malloc(sizeof(MPI_Request) * mpi_size * 2);
        shape[1] = matrix.w;
        for (int i = 0; i < mpi_size; i++) {
            shape[0] = (i == mpi_size - 1) ? matrix.h - (matrix.h / mpi_size) * i : matrix.h / mpi_size;
            MPI_Isend(shape, 2, MPI_UNSIGNED, i, SIZE_TAG, MPI_COMM_WORLD, &requests[2 * i]);
            MPI_Isend(matrix.data + (matrix.h / mpi_size) * i * shape[1], shape[0] * shape[1],
                      MPI_DOUBLE, i, STRIP_TAG, MPI_COMM_WORLD, &requests[2 * i + 1]);
        }
        free(requests);
        rang = rang_mpi_worker(mpi_rank, mpi_size);
    } else {
        rang_mpi_worker(mpi_rank, mpi_size);
    }

    calculation_time = MPI_Wtime() - calculation_time;

    if (mpi_rank == 0) {
        fprintf(stderr, "%d,%u,%u,%lg,%s,%d\n", mpi_size, matrix.h, matrix.w, calculation_time, argv[1], rang);
        matrix_free(&matrix);
    }
    MPI_Finalize();
    return 0;
}

int matrix_load(Matrix *matrix, const char *filename) {
    FILE *fin = fopen(filename, "r");
    if (!fin) {
        return 1;  // can't open file
    }
    matrix_free(matrix);
    fscanf(fin, "%u%u", &matrix->h, &matrix->w); // NOLINT
    matrix->data = calloc(matrix->h * matrix->w, sizeof(double));
    for (unsigned i = 0; i < matrix->h; i++) {
        for (unsigned j = 0; j < matrix->w; j++) {
            fscanf(fin, "%lg", &matrix->data[i * matrix->w + j]); // NOLINT
        }
    }
    fclose(fin);
    return 0;
}

int matrix_save(Matrix *matrix, const char *filename) {
    FILE *fout = fopen(filename, "w");
    if (!fout) {
        return 1;  // can't open file
    }
    fprintf(fout, "%u %u\n", matrix->h, matrix->w);
    for (unsigned i = 0; i < matrix->h; i++) {
        for (unsigned j = 0; j < matrix->w; j++) {
            if (j > 0) fprintf(fout, " ");
            fprintf(fout, "%lg", matrix->data[i * matrix->w + j]);
        }
        fprintf(fout, "\n");
    }
    fclose(fout);
    return 0;
}

int matrix_free(Matrix *matrix) {
    if (matrix->data) {
        free(matrix->data);
        matrix->data = 0;
    }
    return 0;
}

int rang_mpi_worker(int mpi_rank, int mpi_size) {
    MPI_Status status;  // unused
    unsigned shape[2];
    Matrix m = {0};

    // Get strip size
    MPI_Recv(shape, 2, MPI_UNSIGNED, 0, SIZE_TAG, MPI_COMM_WORLD, &status);
    m.h = shape[0];
    m.w = shape[1];
    printf("Worker %d: (%u, %u)\n", mpi_rank, m.h, m.w);

    // Fill strip with values
    char *used = calloc(m.h, sizeof(char));
    m.data = calloc(m.h * m.w, sizeof(double));
    MPI_Recv(m.data, m.h * m.w, MPI_DOUBLE, 0, STRIP_TAG, MPI_COMM_WORLD, &status);
    double *sub_row = calloc(m.w, sizeof(double));

    // Are you ready?
    MPI_Barrier(MPI_COMM_WORLD);

    int rang = m.w;  // only for mpi_rank == 0
    for (unsigned j = 0; j < m.w; j++) {
        int unused_row = -1;
        int source_mpi_rank = -1;
        for (unsigned i = 0; i < m.h; i++) {
            if (fabs(m.data[i * m.w + j]) > eps && !used[i]) {
                unused_row = i;
                break;
            }
        }

        MPI_Request request;  // unused
        MPI_Isend(&unused_row, 1, MPI_INT, 0, SEARCH_TAG, MPI_COMM_WORLD, &request);
        if (mpi_rank == 0) {
            source_mpi_rank = -1;
            int row_buffer = -1;
            for (int i = 0; i < mpi_size; i++) {  // Read all messages
                MPI_Recv(&row_buffer, 1, MPI_INT, MPI_ANY_SOURCE, SEARCH_TAG, MPI_COMM_WORLD, &status);
                if (row_buffer != -1 && source_mpi_rank == -1) {
                    source_mpi_rank = status.MPI_SOURCE;
                }
            }
            if (source_mpi_rank == -1) {
                rang -= 1;
            }
            MPI_Bcast(&source_mpi_rank, 1, MPI_INT, 0, MPI_COMM_WORLD);  // send to others
        } else {
            MPI_Bcast(&source_mpi_rank, 1, MPI_INT, 0, MPI_COMM_WORLD);  // receive from 0
        }

        if (source_mpi_rank == -1) continue;  // Nothing to subtract
        if (source_mpi_rank == mpi_rank) {
            memcpy(sub_row, &m.data[unused_row * m.w], m.w * sizeof(double));
            used[unused_row] = 1;
        }

        // Send and receive row
        MPI_Bcast(sub_row, m.w, MPI_DOUBLE, source_mpi_rank, MPI_COMM_WORLD);

        // Subtract rows
        for (unsigned i = 0; i < m.h; i++) {
            if (fabs(m.data[i * m.w + j]) > eps && !used[i]) {
                double x = m.data[i * m.w + j] / sub_row[j];
                for (unsigned k = j + 1; k < m.w; k++) {
                    m.data[i * m.w + k] -= x * sub_row[k];
                }
            }
        }
    }

    // Clean up
    free(used);
    matrix_free(&m);
    free(sub_row);
    return rang;
}
